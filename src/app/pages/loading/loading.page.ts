import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.page.html',
  styleUrls: ['./loading.page.scss'],
})
export class LoadingPage implements OnInit {

  loading: HTMLIonLoadingElement;

  constructor(private loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  mostarLoading() {
    this.presentLoadingWithOptions('Cargando...');

    setTimeout(() => {
      this.loading.dismiss();
    }, 2000);
  }

  async presentLoadingWithOptions(message: string) {
    this.loading = await this.loadingCtrl.create({
      // spinner: null,
      // duration: 5000,
      message,
      // translucent: true,
      // cssClass: 'custom-class custom-loading',
      // backdropDismiss: true
    });
    this.loading.present();

    // const { role, data } = await loading.onDidDismiss();
    // console.log('Loading dismissed with role:', role);
  }


}
